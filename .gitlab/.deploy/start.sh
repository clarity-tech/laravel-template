#!/usr/bin/env sh

set -e

echo 'Running start'

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

if [ "$env" == "production" ]; then
    echo "Caching configuration for production env..."
    (cd /var/www/html && php artisan optimize && php artisan event:cache && php artisan view:cache)
fi

if [ "$role" = "app" ]; then

    echo "Running the app in ROLE as SERVER ..."
    echo "Running PHP-fpm and NGINX"
    (php-fpm -D && nginx -g 'daemon off;')

elif [ "$role" = "queue" ]; then

    echo "Running the app in ROLE as horizon queue..."
    php /var/www/html/artisan migrate:status
    echo "Running migration"
    php /var/www/html/artisan migrate --force --no-interaction
    php /var/www/html/artisan horizon

elif [ "$role" = "scheduler" ]; then

    echo "Running the app in ROLE as Scheduler ..."
    while [ true ]
    do
      php /var/www/html/artisan schedule:run --verbose --no-interaction &
      sleep 60
    done

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
