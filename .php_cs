<?php

/**
 * Config for PHP-CS-Fixer ver2
 */
$rules = [
    'psr0' => false,
    '@PSR2' => true,
    'array_indentation' => true,
    'array_syntax' => ['syntax' => 'short'],
    'combine_consecutive_unsets' => true,
    'method_separation' => true,
    'no_multiline_whitespace_before_semicolons' => true,
    'single_quote' => true,

    'binary_operator_spaces' => [
        'align_double_arrow' => false,
        'align_equals' => false,
    ],
    // 'blank_line_after_opening_tag' => true,
    // 'blank_line_before_return' => true,
    'no_short_echo_tag' => true,
    'no_unused_imports' => true,
    'no_useless_else' => true,
    'not_operator_with_successor_space' => true,
    'simplified_null_return' => false,
    'ordered_imports' => ['sort_algorithm' => 'alpha'],

    'blank_line_after_namespace' => true,
    //'braces' => true,
    'braces' => [
        'allow_single_line_closure' => true,
    ],
    'class_definition' => true,
    'concat_space' => ['spacing' => 'one'],
    'declare_equal_normalize' => true,
    'function_typehint_space' => true,
    'hash_to_slash_comment' => true,
    'include' => true,
    'lowercase_cast' => true,
    // 'native_function_casing' => true,
    // 'new_with_braces' => true,
    // 'no_blank_lines_after_class_opening' => true,
    // 'no_blank_lines_after_phpdoc' => true,
    // 'no_empty_comment' => true,
    // 'no_empty_phpdoc' => true,
    // 'no_empty_statement' => true,
    'no_extra_consecutive_blank_lines' => [
        'curly_brace_block',
        'extra',
        'parenthesis_brace_block',
        'square_brace_block',
        'throw',
        'use',
    ],
    'elseif' => true,
    'function_declaration' => true,
    'indentation_type' => true,
    'line_ending' => true,
    'lowercase_constants' => true,
    'lowercase_keywords' => true,
    'method_argument_space' => [
        'ensure_fully_multiline' => true,
    ],
    'no_break_comment' => true,
    'no_closing_tag' => true,
    'no_spaces_after_function_name' => true,
    'no_spaces_inside_parenthesis' => true,
    'no_trailing_whitespace' => true,
    'no_trailing_whitespace_in_comment' => true,
    'single_blank_line_at_eof' => true,
    'single_class_element_per_statement' => [
        'elements' => ['property'],
    ],
    'single_import_per_statement' => true,
    'single_line_after_imports' => true,
    'switch_case_semicolon_to_colon' => true,
    'switch_case_space' => true,
    'visibility_required' => true,
    'encoding' => true,
    'full_opening_tag' => true,
    'single_blank_line_before_namespace' => true,
    // 'space_after_semicolon' => true,
    // 'standardize_not_equals' => true,
    'ternary_operator_spaces' => true,
    // 'trailing_comma_in_multiline_array' => true,
    'trim_array_spaces' => true,
    'unary_operator_spaces' => true,
    'whitespace_after_comma_in_array' => true,
];
$excludes = [
    // add exclude project directory
    'vendor',
    'node_modules',
    'bootstrap/cache'
];

return PhpCsFixer\Config::create()
    ->setRules($rules)
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__)
            ->exclude($excludes)
            ->notName('README.md')
            ->notName('*.xml')
            ->notName('*.yml')
            ->name('*.php')
            ->ignoreDotFiles(true)
            ->ignoreVCS(true)
    )
    ->setLineEnding("\n");
