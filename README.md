Pipeline STATUS

MASTER [![pipeline status](https://gitlab.com/%{project_path}/%{project_id}/badges/develope/pipeline.svg)](https://gitlab.com/convertifier/shopify-app/-/commits/master)

STAGING [![pipeline status](https://gitlab.com/%{project_path}/%{project_id}/badges/develope/pipeline.svg)](https://gitlab.com/convertifier/shopify-app/-/commits/staging)

Develope [![pipeline status](https://gitlab.com/%{project_path}/%{project_id}/badges/develope/pipeline.svg)](https://gitlab.com/convertifier/shopify-app/-/commits/develope)

Code Coverage report

MASTER [![coverage report](https://gitlab.com/%{project_path}/%{project_id}/badges/develope/coverage.svg)](https://gitlab.com/convertifier/shopify-app/-/commits/master)

STAGING [![coverage report](https://gitlab.com/%{project_path}/%{project_id}/badges/develope/coverage.svg)](https://gitlab.com/convertifier/shopify-app/-/commits/staging)

Develope [![coverage report](https://gitlab.com/%{project_path}/%{project_id}/badges/develope/coverage.svg)](https://gitlab.com/convertifier/shopify-app/-/commits/develope)

## About Project Scaffold

This is based on laravel 7 without the auth scaffold included
and only vue installed as dependents and telescope installed


Laravel is accessible, powerful, and provides tools required for large, robust applications.


First project setup GITLAB SPECIFIC
Go to > Settings > General pipelines >Custom CI configuration path
update to this

`.gitlab/.gitlab-ci.yml`

and under > Test coverage parsing
update to this

`^\s*Lines:\s*\d+.\d+\%`


## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.